# Additional checks needed to build the module
AC_DEFUN([DUNE_TNNMG_CHECKS],[])

# Additional check needed to find the module
AC_DEFUN([DUNE_TNNMG_CHECK_MODULE],[
  DUNE_CHECK_MODULES([dune-tnnmg], [tnnmg/iterationsteps/tnnmgstep.hh])
])
