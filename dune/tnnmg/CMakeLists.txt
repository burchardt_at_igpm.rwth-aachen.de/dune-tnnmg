add_subdirectory("functionals")
add_subdirectory("iterationsteps")
add_subdirectory("linearsolvers")
add_subdirectory("localsolvers")
add_subdirectory("nonlinearities")
add_subdirectory("problem-classes")
add_subdirectory("projections")
add_subdirectory("solvers")
add_subdirectory("test")


install(FILES
    concepts.hh
    typetraits.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/tnnmg)
