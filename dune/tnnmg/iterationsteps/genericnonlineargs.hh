#ifndef GENERIC_NONLINEAR_GS_HH
#define GENERIC_NONLINEAR_GS_HH

#include <dune/solvers/iterationsteps/iterationstep.hh>

/** \brief Generic nonlinear Gauss-Seidel smoothing step
 *
 *  \param TNNMGProblemType Problem class implementing the IterateObject
 */

template<class TNNMGProblemType>
class GenericNonlinearGS : public IterationStep<typename TNNMGProblemType::VectorType>
{
    public:

        typedef typename TNNMGProblemType::VectorType VectorType;

        //! default constructor
        GenericNonlinearGS() {};

        //! destructor
        ~GenericNonlinearGS() {};

        /** \brief sets the problem and solution vector
         *
         *  \param x vector which holds the initial iterate (will be overwritten by new iterates)
         *  \param problem object of TNNMGProblemType implementing the IterateObject
         */
        void setProblem(VectorType& x, TNNMGProblemType& problem)
        {
            this->x_ = &x;
            this->problem = &problem;
        };

        /** \brief makes one Gauss-Seidel iteration step for the given problem
         *
         *  Upon completion x_ holds the new iterate accessible by getSol().
         *
         */
        void iterate()
        {
            typedef typename TNNMGProblemType::IterateObject IterateObject;
            VectorType& x = *x_;

            IterateObject iterateObject = problem->getIterateObject();

            iterateObject.setIterate(x);
            for(size_t i=0; i<x.size(); ++i)
            {
                iterateObject.solveLocalProblem(x[i], i, (*this->ignoreNodes_)[i]);
                iterateObject.updateIterate(x[i], i);
            }
        };

    private:

        TNNMGProblemType* problem;
        using IterationStep<VectorType>::x_;
};

#endif

