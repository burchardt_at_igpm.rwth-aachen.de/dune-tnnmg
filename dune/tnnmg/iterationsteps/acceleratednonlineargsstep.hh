// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_ITERATIONSTEPS_ACCELERATEDNONLINEARGSSTEP_HH
#define DUNE_TNNMG_ITERATIONSTEPS_ACCELERATEDNONLINEARGSSTEP_HH


#include <dune/solvers/iterationsteps/iterationstep.hh>

#include <dune/tnnmg/iterationsteps/nonlineargsstep.hh>



namespace Dune {
namespace TNNMG {



/**
 * \brief A nonlinear Gauss-Seidel step
 *
 * \tparam V Vector type storing the iterate
 * \tparam F Functional to minimize
 * \tparam LS Local solver type
 * \tparam BV Bit-vector type for marking ignored components
 */
template<class V, class F, class LS, class A, class BV = Dune::BitSetVector<V::block_type::dimension>>
class AcceleratedNonlinearGSStep :
  public IterationStep<V, BV>
{
  using Base = IterationStep<V, BV>;

public:

  using Vector = typename Base::Vector;
  using BitVector = typename Base::BitVector;
  using Functional = F;

  using LocalSolver = LS;
  using NonlinearSmoother = NonlinearGSStep<Functional, LocalSolver, BitVector>;

  using AccelerationStep = A;

  //! default constructor
  template<class LS_T, class A_T>
  AcceleratedNonlinearGSStep(const Functional& f, Vector& x, LS_T&& localSolver, A_T&& accelerationStep) :
    Base(x),
    f_(&f),
    nonlinearSmoother_(f, x, std::forward<LS_T>(localSolver)),
    preSmoothingSteps_(1),
    postSmoothingSteps_(0),
    accelerationStep_(accelerationStep)
  {}

  //! destructor
  ~AcceleratedNonlinearGSStep()
  {}

  using Base::getIterate;

  void preprocess() override
  {
    nonlinearSmoother_.setIgnore(this->ignore());
    nonlinearSmoother_.preprocess();
  }

  void setPreSmoothingSteps(std::size_t i)
  {
    preSmoothingSteps_ = i;
  }

  void setPostSmoothingSteps(std::size_t i)
  {
    postSmoothingSteps_ = i;
  }

  /**
   * \brief Do one Gauss-Seidel step
   */
  void iterate() override
  {

    const auto& f = *f_;
    const auto& ignore = (*this->ignoreNodes_);
    auto& x = *getIterate();

    for (std::size_t i=0; i<preSmoothingSteps_; ++i)
        nonlinearSmoother_.iterate();

    accelerationStep_(f, x, ignore);

    for (std::size_t i=0; i<postSmoothingSteps_; ++i)
        nonlinearSmoother_.iterate();
  }

  const AccelerationStep& accelerationStep() const
  {
    return accelerationStep_;
  }

private:

  const Functional* f_;

  NonlinearSmoother nonlinearSmoother_;
  std::size_t preSmoothingSteps_;
  std::size_t postSmoothingSteps_;

  AccelerationStep accelerationStep_;
};



} // end namespace TNNMG
} // end namespace Dune




#endif // DUNE_TNNMG_ITERATIONSTEPS_ACCELERATEDNONLINEARGSSTEP_HH
