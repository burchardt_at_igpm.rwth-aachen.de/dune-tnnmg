// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_ITERATIONSTEPS_TNNMGACCELERATION_HH
#define DUNE_TNNMG_ITERATIONSTEPS_TNNMGACCELERATION_HH

#include <memory>
#include <cmath>

#include <dune/solvers/common/resize.hh>



namespace Dune {
namespace TNNMG {



/**
 * \brief TNNMG acceleration step for AcceleratedNonlinearGSStep
 *
 * This implements TNNMG-like accelerations for nonlinear Gauss-Seidel
 * methods. Notice that this is parametrized wrt the functional and
 * all the substeps needed in a TNNMG coarse correction
 *
 * Notice, that this will internally create a Linearization object on
 * demand and reuse it on all applications of the acceleration step.
 *
 * Because this class is itself used as callback in the
 * AcceleratedNonlinearGSStep it should have value semantics.
 * Hence it is designed to be cheap to copy.
 *
 * \tparam F Type of functional to minimize
 * \tparam BV Type of bit vectors used to mark ignored components
 * \tparam CL Type of the constrained linearization used for coarse corrections
 * \tparam LS Type of linear solver callback used for the coarse correction
 * \tparam DP Type of defect projection callback
 * \tparam LSS Type of solver callback for scalar line search problems
 */
template<class F, class BV, class CL, class LS, class DP, class LSS>
class TNNMGAcceleration
{
    using Vector = typename F::Vector;
    using BitVector = BV;
    using Linearization = CL;
    using LinearSolver = LS;
    using DefectProjection = DP;
    using LineSearchSolver = LSS;
    using ConstrainedVector = typename Linearization::ConstrainedVector;

public:

    /**
     * \brief Create TNNMGAcceleration
     *
     * Notice that all callbacks passed to the constructor will be stored
     * by value and should thus be cheap to copy.
     *
     * \param linearSolver This is a callback used to solve the constrained linearized system
     * \param projection This is a callback used to compute a projection into a defect-admissible set
     * \param lineSolver This is a callback used to minimize a directional restriction of the functional for computing a damping parameter
     */
    TNNMGAcceleration(const LinearSolver& linearSolver, const DefectProjection& projection, const LineSearchSolver& lineSolver) :
        linearSolver_(linearSolver),
        projection_(projection),
        lineSolver_(lineSolver)
    {}

    /**
     * \brief Apply acceleration step
     *
     * Apply the acceleration step to compute a correction
     * for the given functional at given position and subject
     * to ignore-constraints.
     *
     * \param f Functional to minimize
     * \param x Current iterate that should be corrected
     * \param ignore Bit vector marking components to ignore
     */
    void operator()(const F& f, Vector& x, const BitVector& ignore)
    {
        if (not linearization_)
            linearization_ = std::make_shared<Linearization>(f, ignore);

        linearization_->bind(x);

        auto&& A = linearization_->hessian();
        auto&& r = linearization_->negativeGradient();

        Dune::Solvers::resizeInitializeZero(correction_, x);
        Dune::Solvers::resizeInitializeZero(constrainedCorrection_, r);

        linearSolver_(constrainedCorrection_, A, r);

        linearization_->extendCorrection(constrainedCorrection_, correction_);

        projection_(f, x, correction_);

        // damp
        auto fv = directionalRestriction(f, x, correction_);
        dampingFactor_ = 0;
        lineSolver_(dampingFactor_, fv, false);
        if (std::isnan(dampingFactor_))
            dampingFactor_ = 0;
        correction_ *= dampingFactor_;

        x += correction_;
    }

    /**
     * \brief Export the last computed damping factor
     */
    double lastDampingFactor() const
    {
        return dampingFactor_;
    }

    /**
     * \brief Export the last used linearization
     */
    const Linearization& linearization() const
    {
        return *linearization_;
    }

private:
    LinearSolver linearSolver_;
    DefectProjection projection_;
    LineSearchSolver lineSolver_;
    ConstrainedVector constrainedCorrection_;
    Vector correction_;
    std::shared_ptr<Linearization> linearization_;
    double dampingFactor_;
};



} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_ITERATIONSTEPS_TNNMGACCELERATION_HH
