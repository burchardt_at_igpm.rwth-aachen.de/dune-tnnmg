#ifndef PRECONFIGURED_TNNMG_STEP_HH
#define PRECONFIGURED_TNNMG_STEP_HH

#include "dune/common/parametertree.hh"

#include "dune/solvers/norms/energynorm.hh"

#include "dune/solvers/transferoperators/multigridtransfer.hh"
#include "dune/solvers/transferoperators/compressedmultigridtransfer.hh"
#include "dune/solvers/transferoperators/densemultigridtransfer.hh"
#include "dune/solvers/iterationsteps/multigridstep.hh"
#include "dune/solvers/iterationsteps/truncatedblockgsstep.hh"

#include "dune/tnnmg/problem-classes/convexproblem.hh"
#include "dune/tnnmg/problem-classes/blocknonlineartnnmgproblem.hh"
#include "dune/tnnmg/iterationsteps/tnnmgstep.hh"
#include "dune/tnnmg/iterationsteps/genericnonlineargs.hh"

//! \todo Please doc me!
template <class ConvexProblemType>
class BlockNonlinearTNNMGStep :
    public TruncatedNonsmoothNewtonMultigrid< BlockNonlinearTNNMGProblem<ConvexProblemType>,
        GenericNonlinearGS< BlockNonlinearTNNMGProblem<ConvexProblemType> > >
{
    public:
        typedef BlockNonlinearTNNMGProblem<ConvexProblemType> TNNMGProblemType;
        typedef GenericNonlinearGS<TNNMGProblemType> SmootherType;
        typedef TruncatedNonsmoothNewtonMultigrid< TNNMGProblemType, SmootherType > TNNMGType;
        typedef typename TNNMGType::Linearization Linearization;

        typedef MultigridStep<typename Linearization::MatrixType, typename Linearization::VectorType> LinearMGStep;
        typedef TruncatedBlockGSStep<typename Linearization::MatrixType, typename Linearization::VectorType> LinearSmoother;

        typedef LoopSolver<typename Linearization::VectorType> BaseSolver;
        typedef EnergyNorm<typename Linearization::MatrixType, typename Linearization::VectorType> BaseEnergyNorm;

        typedef MultigridTransfer< typename Linearization::VectorType, typename Linearization::BitVectorType, typename Linearization::MatrixType > Transfer;
        typedef CompressedMultigridTransfer< typename Linearization::VectorType, typename Linearization::BitVectorType, typename Linearization::MatrixType > CompressedTransfer;
        typedef DenseMultigridTransfer< typename Linearization::VectorType, typename Linearization::BitVectorType, typename Linearization::MatrixType > DenseTransfer;

        typedef std::vector<CompressedTransfer*> CompressedTransferHierarchy;
        typedef std::vector<DenseTransfer*> DenseTransferHierarchy;
        typedef std::vector<Transfer*> TransferHierarchy;

        template<class DerivedTransfer>
        BlockNonlinearTNNMGStep(const Dune::ParameterTree& config, ConvexProblemType& problem, const std::vector<DerivedTransfer*>& transfer) :
            config_(config),
            tnnmgProblem_(config, problem),
            baseEnergyNorm_(baseSolverStep_),
            baseSolver_(&baseSolverStep_, 100, 1e-8, &baseEnergyNorm_, Solver::QUIET)
        {
            linearMGStep_.setTransferOperators(transfer);

            linearMGStep_.setSmoother(&linearPreSmoother_, &linearPostSmoother_);
            linearMGStep_.basesolver_ = &baseSolver_;

            this->setProblem(problem.u, tnnmgProblem_);
            this->setNonlinearSmoother(smoother_);
            this->setLinearIterationStep(linearMGStep_);
        }

        void setMGType(int coarseSteps, int preSteps, int postSteps)
        {
            linearMGStep_.setMGType(coarseSteps, preSteps, postSteps);
        }

        void setNumberOfLevels(int levels)
        {
            linearMGStep_.setNumberOfLevels(levels);
        }

    private:
        const Dune::ParameterTree& config_;

        TNNMGProblemType tnnmgProblem_;
        SmootherType smoother_;

        LinearMGStep linearMGStep_;
        LinearSmoother linearPreSmoother_;
        LinearSmoother linearPostSmoother_;

        LinearSmoother baseSolverStep_;
        BaseEnergyNorm baseEnergyNorm_;
        BaseSolver baseSolver_;

};
#endif

