#ifndef DUNE_TNNMG_ITERATIONSTEPS_TRUNCATED_NONSMOOTH_NEWTON_MULTIGRID_HH
#define DUNE_TNNMG_ITERATIONSTEPS_TRUNCATED_NONSMOOTH_NEWTON_MULTIGRID_HH

#include <string>
#include <sstream>
#include <vector>
#include <iomanip>

#include <dune/common/timer.hh>

#include <dune/solvers/common/resize.hh>
#include "dune/solvers/iterationsteps/iterationstep.hh"
#include "dune/solvers/iterationsteps/lineariterationstep.hh"
#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/solvers/linearsolver.hh>

namespace Dune {
namespace TNNMG {

/**
 * \brief One iteration of the TNNMG method
 *
 * \tparam F Functional to minimize
 * \tparam BV Bit-vector type for marking ignored components
 */
template<class F, class BV, class Linearization,
                                  class DefectProjection,
                                  class LineSearchSolver>
class TNNMGStep :
  public IterationStep<typename F::Vector, BV>
{
  using Base = IterationStep<typename F::Vector, BV>;

public:

  using Vector = typename F::Vector;
  using ConstrainedVector = typename Linearization::ConstrainedVector;
  using ConstrainedMatrix = typename Linearization::ConstrainedMatrix;
  using BitVector = typename Base::BitVector;
  using ConstrainedBitVector = typename Linearization::ConstrainedBitVector;
  using Functional = F;
  using IterativeSolver = Solvers::IterativeSolver< ConstrainedVector, Solvers::DefaultBitVector_t<ConstrainedVector> >;
  using LinearSolver = Solvers::LinearSolver< ConstrainedMatrix,  ConstrainedVector >;

  /** \brief Constructor with an iterative solver object for the linear correction
   * \param iterativeSolver This is a callback used to solve the constrained linearized system
   * \param projection This is a callback used to compute a projection into a defect-admissible set
   * \param lineSolver This is a callback used to minimize a directional restriction of the functional
   *        for computing a damping parameter
   */
  TNNMGStep(const Functional& f,
            Vector& x,
            std::shared_ptr<IterationStep<Vector,BitVector> > nonlinearSmoother,
            std::shared_ptr<IterativeSolver> iterativeSolver,
            const DefectProjection& projection,
            const LineSearchSolver& lineSolver)
  : Base(x),
    f_(&f),
    nonlinearSmoother_(nonlinearSmoother),
    preSmoothingSteps_(1),
    iterativeSolver_(iterativeSolver),
    projection_(projection),
    lineSolver_(lineSolver)
  {}

  /** \brief Constructor with a linear solver object for the linear correction
   * \param linearSolver This is a callback used to solve the constrained linearized system
   * \param projection This is a callback used to compute a projection into a defect-admissible set
   * \param lineSolver This is a callback used to minimize a directional restriction of the functional
   *        for computing a damping parameter
   */
  TNNMGStep(const Functional& f,
            Vector& x,
            std::shared_ptr<IterationStep<Vector,BitVector> > nonlinearSmoother,
            std::shared_ptr<LinearSolver> linearSolver,
            const DefectProjection& projection,
            const LineSearchSolver& lineSolver)
  : Base(x),
    f_(&f),
    nonlinearSmoother_(nonlinearSmoother),
    preSmoothingSteps_(1),
    linearSolver_(linearSolver),
    projection_(projection),
    lineSolver_(lineSolver)
  {}

  /** \brief Constructor with a LinearIterationStep object for the linear correction
   * \param linearIterationStep This is a callback used to solve the constrained linearized system
   * \param projection This is a callback used to compute a projection into a defect-admissible set
   * \param lineSolver This is a callback used to minimize a directional restriction of the functional
   *        for computing a damping parameter
   */
  TNNMGStep(const Functional& f,
            Vector& x,
            std::shared_ptr<Solvers::IterationStep<Vector,BitVector> > nonlinearSmoother,
            std::shared_ptr<Solvers::LinearIterationStep<ConstrainedMatrix,ConstrainedVector> > linearIterationStep,
            unsigned int noOfLinearIterationSteps,
            const DefectProjection& projection,
            const LineSearchSolver& lineSolver)
  : Base(x),
    f_(&f),
    nonlinearSmoother_(nonlinearSmoother),
    preSmoothingSteps_(1),
    linearIterationStep_(linearIterationStep),
    noOfLinearIterationSteps_(noOfLinearIterationSteps),
    projection_(projection),
    lineSolver_(lineSolver)
  {}

  //! destructor
  ~TNNMGStep()
  {}

  using Base::getIterate;

  void preprocess() override
  {
    nonlinearSmoother_->setIgnore(this->ignore());
    nonlinearSmoother_->preprocess();
  }

  void setPreSmoothingSteps(std::size_t i)
  {
    preSmoothingSteps_ = i;
  }

  /**
   * \brief Do one TNNMG step
   */
  void iterate() override
  {

    const auto& f = *f_;
    const auto& ignore = (*this->ignoreNodes_);
    auto& x = *getIterate();

    // Nonlinear presmoothing
    for (std::size_t i=0; i<preSmoothingSteps_; ++i)
        nonlinearSmoother_->iterate();

    // Compute constraint/truncated linearization
    if (not linearization_)
      linearization_ = std::make_shared<Linearization>(f, ignore);

    linearization_->bind(x);

    auto&& A = linearization_->hessian();
    auto&& r = linearization_->negativeGradient();

    // Compute inexact solution of the linearized problem
    Solvers::resizeInitializeZero(correction_, x);
    Solvers::resizeInitializeZero(constrainedCorrection_, r);

    // TNNMGStep assumes that the linearization and the solver for the
    // linearized problem will not use the ignoreNodes field
    auto emptyIgnore = ConstrainedBitVector();

    Solvers::resizeInitialize(emptyIgnore, constrainedCorrection_, false);

    // Do the linear correction with a LinearIterationStep object
    if (linearIterationStep_)
    {
      linearIterationStep_->setIgnore(emptyIgnore);
      linearIterationStep_->setProblem(A, constrainedCorrection_, r);
      linearIterationStep_->preprocess();

      for (unsigned int i=0; i<noOfLinearIterationSteps_; i++)
        linearIterationStep_->iterate();
    }
    else if (iterativeSolver_)  // Do the linear correction with an iterative Solver object
    {
      // Hand the linear problem to the iterative solver.
      // The IterationStep member needs to be a LinearIterationStep,
      // so we can give it the matrix.
      using LinearIterationStepType = Solvers::LinearIterationStep<std::decay_t<decltype(A)>,
                                                                  typename Linearization::ConstrainedVector,
                                                                  decltype(emptyIgnore) >;

      LinearIterationStepType* linearIterationStep;

      auto iterativeSolver = std::dynamic_pointer_cast<Solvers::IterativeSolver<typename Linearization::ConstrainedVector> >(iterativeSolver_);
      if (iterativeSolver)
      {
        iterativeSolver->getIterationStep().setIgnore(emptyIgnore);
        linearIterationStep = dynamic_cast<LinearIterationStepType*>(&(iterativeSolver->getIterationStep()));
      } else
        DUNE_THROW(Exception, "Linear solver has to be an IterativeSolver!");

      if (linearIterationStep)
        linearIterationStep->setProblem(A, constrainedCorrection_, r);
      else
        DUNE_THROW(Exception, "Linear solver does not accept matrices!");


      iterativeSolver_->preprocess();
      iterativeSolver_->solve();
    }
    else // Do the linear correction with a linear Solver object
    {
      linearSolver_->setProblem(A,constrainedCorrection_, r);
      linearSolver_->preprocess();
      linearSolver_->solve();
    }

    linearization_->extendCorrection(constrainedCorrection_, correction_);

    // Project onto admissible set
    projection_(f, x, correction_);

    // Line search
    auto fv = directionalRestriction(f, x, correction_);
    dampingFactor_ = 0;
    lineSolver_(dampingFactor_, fv, false);
    if (std::isnan(dampingFactor_))
      dampingFactor_ = 0;
    correction_ *= dampingFactor_;

    x += correction_;
  }

  /**
   * \brief Export the last computed damping factor
   */
  double lastDampingFactor() const
  {
    return dampingFactor_;
  }

  /**
   * \brief Export the last used linearization
   */
  const Linearization& linearization() const
  {
    return *linearization_;
  }

private:

  const Functional* f_;

  std::shared_ptr<IterationStep<Vector,BitVector> > nonlinearSmoother_;
  std::size_t preSmoothingSteps_;

  std::shared_ptr<Linearization> linearization_;

  // The following members cannot all be used at once:
  // either we have a Dune::Solvers::IterativeSolver that implements the linear correction...
  std::shared_ptr<IterativeSolver> iterativeSolver_;

  // or we have a Dune::Solvers::LinearSolver that implements the linear correction...
  std::shared_ptr<LinearSolver> linearSolver_;

  // ... or we have a mere LinearIterationStep, together with a number of times
  // it is supposed to be called.  You cannot have more than one at once.
  std::shared_ptr<LinearIterationStep<typename Linearization::ConstrainedMatrix,typename Linearization::ConstrainedVector> > linearIterationStep_;
  unsigned int noOfLinearIterationSteps_;

  typename Linearization::ConstrainedVector constrainedCorrection_;
  Vector correction_;
  DefectProjection projection_;
  LineSearchSolver lineSolver_;
  double dampingFactor_;
};


} // end namespace TNNMG
} // end namespace Dune

#endif
