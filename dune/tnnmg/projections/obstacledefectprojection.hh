// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_PROJECTIONS_OBSTACLEDEFECTPROJECTION_HH
#define DUNE_TNNMG_PROJECTIONS_OBSTACLEDEFECTPROJECTION_HH

#include <dune/common/hybridutilities.hh>

#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/common/typetraits.hh>

namespace Dune {
namespace TNNMG {

  namespace {
    template <class Obstacle, class Vector,
              bool IsNumber = Dune::IsNumber<Vector>::value>
    struct ObstacleDefectProjectionHelper {
      static constexpr void apply(const Obstacle& lo, const Obstacle& uo,
                                  const Vector& x, Vector& v) {
        using namespace Dune::Hybrid;
        forEach(integralRange(Hybrid::size(x)), [&](auto&& i) {
          auto const& lo_i = lo[i];
          auto const& uo_i = uo[i];
          auto const& x_i = x[i];
          auto& v_i = v[i];
          ObstacleDefectProjectionHelper<
              std::decay_t<decltype(lo_i)>,
              std::decay_t<decltype(x_i)>>::apply(lo_i, uo_i, x_i, v_i);
        });
      }
    };
    template <class Obstacle, class Vector>
    struct ObstacleDefectProjectionHelper<Obstacle, Vector, true> {
      static constexpr void apply(const Obstacle& lo, const Obstacle& uo,
                                  const Vector& x, Vector& v) {
        if (v < lo - x)
          v = lo - x;
        if (v > uo - x)
          v = uo - x;
      }
    };
  }

  struct ObstacleDefectProjection {
    template <class Functional, class Vector>
    constexpr void operator()(const Functional& f, const Vector& x, Vector& v) {
      auto const& lo = f.lowerObstacle();
      auto const& uo = f.upperObstacle();
      ObstacleDefectProjectionHelper<std::decay_t<decltype(lo)>, Vector>::apply(
          lo, uo, x, v);
    }
  };

} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_PROJECTIONS_OBSTACLEDEFECTPROJECTION_HH
