// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_LOCALSOLVERS_SIMPLEXSOLVER_HH
#define DUNE_TNNMG_LOCALSOLVERS_SIMPLEXSOLVER_HH

#include <algorithm>
#include <cmath>
#include <functional>
#include <numeric>

#include <dune/matrix-vector/axpy.hh>

#include "../functionals/boxconstrainedquadraticfunctional.hh"

namespace Dune {
namespace TNNMG {

/**
 * \brief A local solver for quadratic minimization problems with lower obstacle
 * where the quadratic part is given by a scaled identity. Additionally a sum
 * constraint is imposed.
 *
 * Note: In order to use it as a solver to compute corrections for a
 * simplex-constrained problem, make sure the iterate already fulfills the sum
 * constraint and you compute corrections with sum constraint 0.
 *
 * The available implementation solves the problem in n*log(n) time, exploiting
 * the decoupling of the energy w.r.t. the coordinate directions.
 *
 * \todo Add generic case where the quadratic part is not given by a scaled
 * identity.
 *
 */
template <class Field = double>
struct SimplexSolver {
  SimplexSolver(Field sum = 0.0)
      : r_(sum) {}

  /**
  * \brief Project b to a (scaled) Gibbs simplex
  * \param x projected vector
  * \param b input vector
  * \param s scaling of the simplex
  *
  * Write the problem as follows:
  *   ( I_N + \partial\phi_N  1_N^T ) (    x   ) = ( b )
  *   (         1_N             0   ) ( lambda ) = ( s )
  * where I_N is the identity matrix with N rows and columns,
  * phi_N is an N-vector of phi_0, the latter denoting the obstacle at 0 and
  * 1_N is the N-vector of ones.
  *
  * We determine lambda by a Schur approach:
  *   For each lambda, (I_N + \partial\phi_N) \ (b - lambda) yields a unique
  *   solution which we can use to replace x in the second equation. Furthermore
  *   the contributions do not couple, so we obtain
  *   s = \sum_i (1 + \partial\phi_0) \ (b_i - lambda).
  *
  *   We sort the values of b by size and successively check if the resulting
  *   intervals do permit a lambda that induces an activity pattern which does
  *   indeed cut off all subsequent (lower) values of b.
  *   Note: This also works in the corner case where b contains duplicate
  *   values, because lambda cannot be larger than the successive element (with
  *   the same value) in these cases -- the interval has width 0.
  *
  * With given lambda, we can compute x easily.
  *
  */
  template <class X, class R>
  static auto projectToSimplex(X& x, X b, R s = 1.0) {
    if (s == 0.0) {
      x = 0.0;
      return;
    }
    assert(s > 0.0 && "Sum constraint must be non-negative.");

    size_t N = b.size();
    assert(N > 0 && "Vector has zero size.");

    // determine lambda by successively checking the intervals of b
    R lambda;
    x = b; // alienate x for a sorted version of b
    std::sort(x.begin(), x.end(), std::greater<R>());
    s *= -1.0;
    for (size_t i = 0; i < N; ++i) {
      s += x[i]; //
      lambda = s / (i + 1);
      if (x[i + 1] < lambda)
        break;
    }

    // compute x
    for (size_t j = 0; j < N; ++j)
      x[j] = std::max(b[j] - lambda, R(0));
  }

  /**
   * \brief Solve a quadratic minimization problem with lower obstacle where
   * the quadratic part is given by a scaled identity. Additionally, a sum
   * constraint is imposed on the solution.
   * \param x the minimizer
   * \param f the quadratic functional with lower obstacle
   * \param ignore ignore nodes
   *
   *  This determines x for the following system (1).
   *  The equivalence transformations (2),(3) show what is implemented.
   *
   *  (1a) x = argmin 0.5*<Av,v> - <b,v> where
   *  (1b) \sum v_i = r
   *  (1c) v_i \geq l_i
   *
   *  Divide energy by a>0.
   *  Set c := 1/a * b.
   *  (2a) x = argmin 0.5*<v,v> - <c,v> where
   *  (2b) \sum v_i = r
   *  (2c) v_i \geq l_i
   *
   *  Transform w := v - l.
   *  Set d := c - l, s = r - \sum l_i, const = 0.5*<l,l> - <c,l>.
   *  (3a) x = l + argmin 0.5*<w,w> - <d,w> + const
   *  (3b) \sum w_i = s
   *  (3c) v_i \geq 0
   *
   */
  template <class X, class K, int n, class V, class L, class U, class R,
            class Ignore>
  auto operator()(X&& x, BoxConstrainedQuadraticFunctional<
                             ScaledIdentityMatrix<K, n>&, V, L, U, R>& f,
                  Ignore& ignore) const {
    if (ignore.all())
      return;
    if (ignore.any())
      DUNE_THROW(NotImplemented, "All or no ignore nodes should be set.");
    assert(ignore.none());

    const auto& a = f.quadraticPart().scalar();
    if (a <= 0.0)
      DUNE_THROW(MathError, "ScaledIdentity scaling must be positive.");

    for (auto&& ui : f.upperObstacle())
      assert(std::isinf(ui) && "Upper obstacle must be infinity.");

    auto&& l = f.lowerObstacle();
    auto s = std::accumulate(l.begin(), l.end(), r_, std::minus<R>());
    auto d = l;
    d *= -1.0;
    Dune::MatrixVector::addProduct(d, 1.0 / a, f.linearPart());
    projectToSimplex(x, d, s);
    x += l;
  }

private:
  const Field r_;
};

} // end namespace TNNMG
} // end namespace Dune

#endif // DUNE_TNNMG_LOCALSOLVERS_SIMPLEXSOLVER_HH
