// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_LINEARSOLVERS_MGSOLVER_HH
#define DUNE_TNNMG_LINEARSOLVERS_MGSOLVER_HH

#include <memory>

#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/common/resize.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/solvers/solver.hh>



namespace Dune {
namespace TNNMG {


/**
 * \brief A linear solver callback using one multigrid step
 *
 * \tparam Matrix Matrix type of linear system
 * \tparam Vector Vector type of linear system
 */
template<class Matrix, class Vector>
class MGSolver
{
  using BitVector = Dune::Solvers::DefaultBitVector_t<Vector>;
  using MGStep = MultigridStep<Matrix, Vector, BitVector>;

public:

  /**
   * \brief Construct V-cycle MGSolver
   *
   * \param transferOperators A vector of transfer operators to be used in the multigrid method
   * \param preSmoother Pre-smoother for the multigrid method
   * \param postSmoother Post-smoother for the multigrid method
   * \param pre Number of pre-smoothing steps
   * \param post Number of post-smoothing steps
   */
  template<class TransferOperators, class PreSmoother, class PostSmoother>
  MGSolver(const TransferOperators& transferOperators, PreSmoother& preSmoother, PostSmoother& postSmoother, unsigned int pre, unsigned int post)
  {
    mgStep_.setMGType(1, pre, post);
    mgStep_.setSmoother(&preSmoother, &postSmoother);
    mgStep_.setTransferOperators(transferOperators);
  }

  /**
   * \brief Construct V-cycle MGSolver
   *
   * \param transferOperators A vector of transfer operators to be used in the multigrid method
   * \param preSmoother Pre-smoother for the multigrid method
   * \param postSmoother Post-smoother for the multigrid method
   * \param pre Number of pre-smoothing steps
   * \param post Number of post-smoothing steps
   * \param coarseSolver
   */
  template<class TransferOperators, class PreSmoother, class PostSmoother>
  MGSolver(const TransferOperators& transferOperators, PreSmoother& preSmoother, PostSmoother& postSmoother, unsigned int pre, unsigned int post,
           std::shared_ptr<Solver> coarseSolver)
  {
    mgStep_.setMGType(1, pre, post);
    mgStep_.setSmoother(&preSmoother, &postSmoother);
    mgStep_.setTransferOperators(transferOperators);
    mgStep_.basesolver_ = coarseSolver.get();
  }

  template<class M>
  void operator()(Vector& x, M&& m, const Vector& r)
  {
    Dune::Solvers::resizeInitialize(ignore_, x, false);
    mgStep_.setIgnore(ignore_);
    mgStep_.setProblem(m, x, r);
    mgStep_.preprocess();
    mgStep_.iterate();
  }

private:
  MGStep mgStep_;
  BitVector ignore_;
};



} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_LINEARSOLVERS_MGSOLVER_HH


