// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_LINEARSOLVERS_FASTAMGSOLVER_HH
#define DUNE_TNNMG_LINEARSOLVERS_FASTAMGSOLVER_HH

#include <memory>

#include <dune/istl/solvers.hh>
#include <dune/istl/paamg/fastamg.hh>



namespace Dune {
namespace TNNMG {



template<class Matrix, class Vector>
class FastAMGSolver
{
  using Operator = Dune::MatrixAdapter<Matrix,Vector,Vector>;
  using Criterion = Dune::Amg::SymmetricCriterion<Matrix, Dune::Amg::RowSum>;
  using AMG = Dune::Amg::FastAMG<Operator, Vector>;
  using Parameters = Dune::Amg::Parameters;

public:

  template<class M>
  void operator()(Vector& x, M&& m, const Vector& r)
  {
//    if (not amg_)
    {
      matrix_ = std::forward<M>(m);
      Parameters parameters;
      parameters.setDebugLevel(0);
      parameters.setNoPreSmoothSteps(1);
      parameters.setNoPostSmoothSteps(1);
      Criterion criterion;
      criterion.setDebugLevel(0);
      operator_ = std::make_shared<Operator>(matrix_);
      amg_ = std::make_shared<AMG>(*operator_, criterion, parameters);
    }
//    else
//    {
//      matrix_ = std::forward<M>(m);
//      amg_->recalculateHierarchy();
//    }
    x = 0;
    auto mutableR = r;
    amg_->pre(x, mutableR);
    amg_->apply(x, mutableR);
  }

private:
    Matrix matrix_;
    std::shared_ptr<AMG> amg_;
    std::shared_ptr<Operator> operator_;
};



} // end namespace TNNMG
} // end namespace Dune



#endif // DUNE_TNNMG_LINEARSOLVERS_FASTAMGSOLVER_HH
