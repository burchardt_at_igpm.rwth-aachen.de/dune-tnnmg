#ifndef DUNE_TNNMG_FUNCTIONALS_ZEROFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_ZEROFUNCTIONAL_HH

/** \file
 * \brief The constant zero functional
 */

#include <dune/solvers/common/interval.hh>
#include <dune/tnnmg/functionals/nonsmoothconvexfunctional.hh>

namespace Dune {

  namespace TNNMG {

    /** \brief A nonlinearity representing the zero function
     */
    template < class LocalVectorType=Dune::FieldVector<double,1>, class LocalMatrixType=Dune::FieldMatrix<double,1,1> >
    class ZeroFunctional : public NonsmoothConvexFunctional<LocalVectorType, LocalMatrixType>
    {
    public:
      typedef NonsmoothConvexFunctional<LocalVectorType, LocalMatrixType> NonlinearityType;

      using NonlinearityType::block_size;

      typedef typename NonlinearityType::VectorType VectorType;
      typedef typename NonlinearityType::MatrixType MatrixType;
      typedef typename NonlinearityType::IndexSet IndexSet;

      /** \brief Returns zero */
      double operator()(const VectorType& v) const
      {
        return 0.0;
      }

      void addGradient(const VectorType& v, VectorType& gradient) const {}
      void addHessian(const VectorType& v, MatrixType& hessian) const {}
      void addHessianIndices(IndexSet& indices) const {}

      /** \brief Returns the interval \f$ [0,0]\f$ */
      void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
      {
        D[0] = 0.0;
        D[1] = 0.0;
      }

      /** \brief Returns 0 -- the zero functional is as regular as you can get */
      double regularity(int i, double x, int j) const
      {
        return 0.0;
      }

      /** \brief Domain of definition in one scalar direction -- the entire real line */
      void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const
      {
        dom[0] = -std::numeric_limits<double>::max();
        dom[1] =  std::numeric_limits<double>::max();
      }

            /** \brief Set the internal position vector u_pos to v.
       *
       * This method is empty, because it is only needed if the nonlinearity
       * does not decouple in the Euclidean directions.
       */
      virtual void setVector(const VectorType& v) {};

      /** \brief Update the (i,j)-th entry of the internal position vector u_pos to x.
       *
       * This method is empty, because it is only needed if the nonlinearity
       * does not decouple in the Euclidean directions.
       *
       * \param i global index
       * \param j local index
       * \param x new value of the entry (u_pos)_(i,j)
       */
      virtual void updateEntry(int i, double x, int j) {};

    };

  }

}

#endif

