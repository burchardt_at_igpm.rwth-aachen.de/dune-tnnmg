#ifndef DUNE_TNNMG_FUNCTIONALS_STRONGLYCONVEXFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_STRONGLYCONVEXFUNCTIONAL_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/deprecated.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrix.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/solvers/operators/nulloperator.hh>

#include <dune/tnnmg/problem-classes/nonlinearity.hh>
#include <dune/tnnmg/functionals/convexfunctional.hh>

namespace Dune {

  namespace TNNMG {

    /** \brief Functional consisting of a quadratic part and a convex part
     *
     * Any strongly convex functional can be (non-uniquely) decomposed into a quadratic part,
     * and a convex remainder.  This class stores such a decomposition.
     *
     *  \tparam NonlinearityTypeTEMPLATE The type used to implement the nonlinearity
     *  \tparam MatrixTypeTEMPLATE The type used for the matrix of the quadratic part
     *  \tparam LowRankFactorTypeTEMPLATE The type used for the matrix used to reconstruct a filled in low rank matrix (i.e. \f$ M = mm^T\f$). The block_type needs to provide a method <tt>block_type::diagonal(int)</tt>.
     */
    template<
        class NonlinearityTypeTEMPLATE=Nonlinearity< Dune::FieldVector<double,1>, Dune::FieldMatrix<double,1,1> >,
        class MatrixTypeTEMPLATE=Dune::BCRSMatrix<Dune::FieldMatrix<double,NonlinearityTypeTEMPLATE::block_size,NonlinearityTypeTEMPLATE::block_size> >,
        class LowRankFactorTypeTEMPLATE=NullOperator<Dune::ScaledIdentityMatrix<double,NonlinearityTypeTEMPLATE::block_size> > >
    class StronglyConvexFunctional
      : public ConvexFunctional<typename NonlinearityTypeTEMPLATE::VectorType>
    {
    public:
      //! export the nonlinearity type
      typedef NonlinearityTypeTEMPLATE NonlinearityType;

      //! export the employed vector type of the nonlinearity
      typedef typename NonlinearityType::VectorType VectorType;

      //! export the employed local vector type of the nonlinearity
      typedef typename NonlinearityType::LocalVectorType LocalVectorType;

      //! export the matrix type for the linear operator
      typedef MatrixTypeTEMPLATE MatrixType;

      //! export the block type of the matrix representing the linear operator
      typedef typename MatrixType::block_type LocalMatrixType;

      //! export the low-rank factor type
      typedef LowRankFactorTypeTEMPLATE LowRankFactorType;

      // the block size of the local matrices/vectors
      static const int block_size = NonlinearityType::block_size;


    protected:
      typedef NullOperator<Dune::ScaledIdentityMatrix<double,block_size> > DefaultNullOperatorType;

      // Store a member to a NullOperator needed if you do not want to explicitly specify one.
      DefaultNullOperatorType nullOperator_;

    public:

      /** \brief Constructor with the problem components
       *
       *  \param a A scalar factor in front of the quadratic part (the quadratic part includes a factor of 1/2 already)
       *  \param A The matrix of the quadratic part
       *  \param am A scalar factor in front of the optional rank-one matrix
       *  \param lowRankFactor A matrix of low rank given by a single line block matrix. The low-rank matrix is lowRankFactor*lowRankFactor^T
       *  \param phi The nonlinearity
       *  \param f The linear functional
       */
      StronglyConvexFunctional(double a, const MatrixType& A, double am, const LowRankFactorType& lowRankFactor, NonlinearityType& phi, const VectorType& f) :
          a(a),
          A(A),
          am(am),
          lowRankFactor_(lowRankFactor),
          phi(phi),
          f(f)
      {};

      /** \brief Constructor with the problem components
       *
       *  \param A The matrix of the quadratic part
       *  \param phi The nonlinearity
       *  \param f The linear functional
       */
      template<class LRFT = LowRankFactorType>
      StronglyConvexFunctional(const MatrixType& A, NonlinearityType& phi, const VectorType& f,
                typename std::enable_if<
                    std::is_same< DefaultNullOperatorType, LRFT>::value
                >::type* dummy=0
                ) :
          a(1.0),
          A(A),
          am(0.0),
          lowRankFactor_(nullOperator_),
          phi(phi),
          f(f)
      {};

      /** \brief Constructor with the problem components
       *
       *  \param a A scalar factor in front of the quadratic part (the quadratic part includes a factor of 1/2 already)
       *  \param A The matrix of the quadratic part
       *  \param phi The nonlinearity
       *  \param f The linear functional
       */
      template<class LRFT = LowRankFactorType>
      StronglyConvexFunctional(double a, const MatrixType& A, NonlinearityType& phi, const VectorType& f,
                typename std::enable_if<
                    std::is_same< DefaultNullOperatorType, LRFT>::value
                >::type* dummy=0
                ) :
          a(a),
          A(A),
          am(0.0),
          lowRankFactor_(nullOperator_),
          phi(phi),
          f(f)
      {};

      /** \brief Evaluates the functional at a given vector u
       *
       *  \param[in] u vector to evaluate the energy at
       *  \returns computed energy
       */
      double operator()(const VectorType& u) const
      {
        VectorType temp = u;
        A.mv(u, temp);

        double energy = a/2.0* (temp*u);

        temp.resize(lowRankFactor_.N());
        lowRankFactor_.mv(u, temp);

        energy += am/2.0* (temp*temp);

        energy -= u*f;

        energy += phi(u);

        return energy;
      }

      /** \brief Evaluates the functional at a given vector u
       *
       *  \param[in] u vector to evaluate the energy at
       *  \returns computed energy
       *  \deprecated Use operator() instead
       */
      double computeEnergy(const VectorType& u) const DUNE_DEPRECATED_MSG("Use operator() instead")
      {
        return operator()(u);
      }

    public:
      //! a scalar factor in front of the quadratic part
      double a;

      //! the matrix of the quadratic part
      const MatrixType& A;

      //! a scalar factor in front of the low-rank term
      double am;

      //! A single line block matrix used to reconstruct a filled in matrix of low rank. The full matrix is lowRankFactor_^T*lowRankFactor_
      const LowRankFactorType& lowRankFactor_;

      //! the nonlinearity
      NonlinearityType& phi;

      //! the linear functional
      const VectorType& f;
    };

  }

}

#endif
