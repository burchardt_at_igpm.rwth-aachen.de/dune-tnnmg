// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_TNNMG_FUNCTIONAL_BCQFCONSTRAINEDLINEARIZATION_HH
#define DUNE_TNNMG_FUNCTIONAL_BCQFCONSTRAINEDLINEARIZATION_HH


#include <cstddef>

#include <dune/common/typetraits.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/matrix-vector/algorithm.hh>



namespace Dune {
namespace TNNMG {



/**
 * \brief A constrained linearization for BoxConstrainedQuadraticFunctional
 */
template<class F, class BV>
class BoxConstrainedQuadraticFunctionalConstrainedLinearization
{
  using This = BoxConstrainedQuadraticFunctionalConstrainedLinearization<F,BV>;

  template<class NV, class NBV, class T>
  static void determineTruncation(const NV& x, const NV& lower, const NV& upper, NBV&& truncationFlags, const T& truncationTolerance)
  {
    namespace H = Dune::Hybrid;
    H::ifElse(IsNumber<NV>(), [&](auto id){
      if ((id(x) <= id(lower)+truncationTolerance) || (id(x) >= id(upper) - truncationTolerance))
        id(truncationFlags) = true;
    }, [&](auto id){
      H::forEach(H::integralRange(H::size(id(x))), [&](auto&& i) {
        This::determineTruncation(x[i], lower[i], upper[i], truncationFlags[i], truncationTolerance);
      });
    });
  }

  template<class NV, class NBV>
  static void truncateVector(NV& x, const NBV& truncationFlags)
  {
    namespace H = Dune::Hybrid;
    H::ifElse(IsNumber<NV>(), [&](auto id){
      if (id(truncationFlags))
        id(x) = 0;
    }, [&](auto id){
      H::forEach(H::integralRange(H::size(id(x))), [&](auto&& i) {
        This::truncateVector(x[i], truncationFlags[i]);
      });
    });
  }

  template<class NM, class RBV, class CBV>
  static void truncateMatrix(NM& A, const RBV& rowTruncationFlags, const CBV& colTruncationFlags)
  {
    namespace H = Dune::Hybrid;
    using namespace Dune::MatrixVector;
    H::ifElse(IsNumber<NM>(), [&](auto id){
      if(id(rowTruncationFlags) or id(colTruncationFlags))
        A = 0;
    }, [&](auto id){
      H::forEach(H::integralRange(H::size(id(rowTruncationFlags))), [&](auto&& i) {
        auto&& Ai = A[i];
        sparseRangeFor(Ai, [&](auto&& Aij, auto&& j) {
            This::truncateMatrix(Aij, rowTruncationFlags[i], colTruncationFlags[j]);
        });
      });
    });
  }

public:
  using Matrix = typename F::Matrix;
  using Vector = typename F::Vector;
  using BitVector = BV;
  using ConstrainedMatrix = Matrix;
  using ConstrainedVector = Vector;
  using ConstrainedBitVector = BitVector;


  BoxConstrainedQuadraticFunctionalConstrainedLinearization(const F& f, const BitVector& ignore) :
    f_(f),
    ignore_(ignore),
    truncationTolerance_(1e-10)
  {}

  void bind(const Vector& x)
  {
    negativeGradient_ = derivative(f_)(x);
    negativeGradient_ *= -1;
    hessian_ = derivative(derivative(f_))(x);
    truncationFlags_ = ignore_;

    // determine which components to truncate
    determineTruncation(x, f_.lowerObstacle(), f_.upperObstacle(), truncationFlags_, truncationTolerance_);

    // truncate gradient and hessian
    truncateVector(negativeGradient_, truncationFlags_);
    truncateMatrix(hessian_, truncationFlags_, truncationFlags_);
  }

  void extendCorrection(ConstrainedVector& cv, Vector& v) const
  {
    v = cv;
    truncateVector(v, truncationFlags_);
  }

  const BitVector& truncated() const
  {
    return truncationFlags_;
  }

  const auto& negativeGradient() const
  {
    return negativeGradient_;
  }

  const auto& hessian() const
  {
    return hessian_;
  }

private:
  const F& f_;
  const BitVector& ignore_;

  double truncationTolerance_;

  Vector negativeGradient_;
  Matrix hessian_;
  BitVector truncationFlags_;
};



} // end namespace TNNMG
} // end namespace Dune




#endif // DUNE_TNNMG_FUNCTIONAL_BCQFCONSTRAINEDLINEARIZATION
