// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <config.h>

#include <iostream>

#include <dune/tnnmg/functionals/normfunctional.hh>
#include <dune/tnnmg/functionals/test/functionaltest.hh>

using namespace Dune;

int main(int argc, char** argv)
{
  // Create a norm functional on (R^3)^2 for testing
  const int blocksize = 3;
  using Vector = BlockVector<FieldVector<double,blocksize> >;
  using Weights = Vector;

  // TODO: Improve: we use the same vector type for weights as for coefficient vectors,
  // even though we have only one coefficient per vector block.
  Weights weights = {{2.0,2.0,2.0}, {3.0,3.0,3.0}};

  TNNMG::NormFunctional<Vector> functional(weights);

  // A set of local test points, i.e., values for a single vector block
  std::vector<FieldVector<double,blocksize> > localTestPoints = {{0,0,0},
                                                                 {1,0,0},
                                                                 {0,-2,0},
                                                                 {3.14,3.14,-4}};

  // Create real test points (i.e., block vectors) from the given values for a single block
  std::vector<Vector> testPoints(localTestPoints.size()*localTestPoints.size());
  for (size_t i=0; i<testPoints.size(); i++)
    testPoints[i].resize(weights.size());

  for (size_t i=0; i<localTestPoints.size(); i++)
    for (size_t j=0; j<localTestPoints.size(); j++)
    {
      testPoints[j*localTestPoints.size()+i][0] = localTestPoints[i];
      testPoints[j*localTestPoints.size()+i][1] = localTestPoints[j];
    }

  // Test whether the functional is convex
  TNNMG::testConvexity(functional, testPoints);

  ///////////////////////////////////////////////////////////////////
  //  Test the directional restriction
  ///////////////////////////////////////////////////////////////////

  TNNMG::testDirectionalRestrictionValues(functional, testPoints, {-3, -2, -1, 0, 1, 2, 3});

  TNNMG::testDirectionalRestrictionSubdifferential(functional, testPoints, {-3, -2, -1, 0, 1, 2, 3});

  ///////////////////////////////////////////////////////////////////
  //  Test the coordinate restriction
  ///////////////////////////////////////////////////////////////////

  // TODO: The following code does some shifting, restricting, and evaluating,
  // without any real strategy.  I more systematic way to test this would be
  // highly appreciated!
  Vector probe;

  probe = testPoints[0];  // Just to get the sizes right
  probe = 0;

  std::cout << "unshifted: " << functional(probe) << std::endl;

  Vector offset = probe;
  offset = 1;
  offset = 2;

  auto shifted1 = shift(functional, offset);

  // Restrict to one FieldVector
  auto shifted2ElementPlasticStrain = coordinateRestriction(shifted1, 0);

  auto shifted3ElementPlasticStrain = shift(shifted2ElementPlasticStrain, offset[0]);

  auto shifted3ElementPlasticStrainDirection = coordinateRestriction(shifted3ElementPlasticStrain, 0);

  std::cout << "shifted3ElementPlasticStrainDirection: " << shifted3ElementPlasticStrainDirection(3*offset[0][0]) << std::endl;

#if 0
  // Test whether the functional positive 1-homogeneous
  // We abuse the test points as test directions.
  testHomogeneity(functional, testPoints);

  // Test the first derivative at the given test points
  testGradient(functional, testPoints);

  // Test the first derivative at the given test points
  testHessian(functional, testPoints);

  // Test the partial subdifferential at the given test points
  testSubDiff(functional, testPoints);
#endif
  return 0;
}
