#ifndef DUNE_TNNMG_FUNCTIONALS_CUBEINDICATORFUNCTIONAL_HH
#define DUNE_TNNMG_FUNCTIONALS_CUBEINDICATORFUNCTIONAL_HH

#include <dune/tnnmg/functionals/nonsmoothconvexfunctional.hh>


namespace Dune {

  namespace TNNMG {

    /** \brief The characteristic function of a hyper-rectangle in \f$ (\mathbb{R}^N)^n \f$
     *
     * \tparam LocalVectorType Type used for elements of \f$ \mathbb{R}^N \f$
     * \tparam LocalMatrixType Matrix type for matrices \f$ \mathbb{R}^{N \times N} \f$
     */
    template < class LocalVectorType=Dune::FieldVector<double,1>, class LocalMatrixType=Dune::FieldMatrix<double,1,1> >
    class CubeIndicatorFunctional: public NonsmoothConvexFunctional<LocalVectorType, LocalMatrixType>
    {
    public:
      typedef NonsmoothConvexFunctional<LocalVectorType, LocalMatrixType> NonlinearityType;

      using NonlinearityType::block_size;

      typedef typename NonlinearityType::VectorType VectorType;
      typedef typename NonlinearityType::MatrixType MatrixType;
      typedef typename NonlinearityType::IndexSet IndexSet;


      CubeIndicatorFunctional(const VectorType& lower, const VectorType& upper, double tolerance = 1e-15) :
      tolerance_(tolerance),
      upper_(upper),
      lower_(lower)
      {}

      /** \brief Evaluate the characteristic functional */
      double operator()(const VectorType& v) const
      {
        for(size_t row=0; row<v.size(); ++row)
        {
          for(int j=0; j<block_size; ++j)
          {
            if ((v[row][j] < lower_[row][j]) or (v[row][j] > upper_[row][j]))
              return std::numeric_limits<double>::max();
          }
        }
        return 0.0;
      }

      void addGradient(const VectorType& v, VectorType& gradient) const {}
      void addHessian(const VectorType& v, MatrixType& hessian) const {}
      void addHessianIndices(IndexSet& indices) const {}

      void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
      {
        D[0] = 0.0;
        D[1] = 0.0;

        if (x < lower_[i][j])
        {
          D[0] = -std::numeric_limits<double>::max();
          D[1] = -std::numeric_limits<double>::max();
        }
        if (x <= tolerance_ + lower_[i][j])
        {
          D[0] = -std::numeric_limits<double>::max();
        }
        if (x >= -tolerance_ + upper_[i][j])
        {
          D[1] = std::numeric_limits<double>::max();
        }
        if (x > upper_[i][j])
        {
          D[0] = std::numeric_limits<double>::max();
          D[1] = std::numeric_limits<double>::max();
        }
        return;
      }

      double regularity(int i, double x, int j) const
      {
        if (x <= tolerance_ + lower_[i][j])
          return std::numeric_limits<double>::max();
        if (x >= -tolerance_ + upper_[i][j])
          return std::numeric_limits<double>::max();
        return 0.0;
      }

      void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const
      {
        dom[0] = lower_[i][j];
        dom[1] = upper_[i][j];
        return;
      }

      /** \brief Set the internal position vector u_pos to v.
       *
       * This method is empty, because it is only needed if the nonlinearity
       * does not decouple in the Euclidean directions.
       */
      virtual void setVector(const VectorType& v) {};

      /** \brief Update the (i,j)-th entry of the internal position vector u_pos to x.
       *
       * This method is empty, because it is only needed if the nonlinearity
       * does not decouple in the Euclidean directions.
       *
       * \param i global index
       * \param j local index
       * \param x new value of the entry (u_pos)_(i,j)
       */
      virtual void updateEntry(int i, double x, int j) {};



    private:
      const double tolerance_;

      const VectorType& upper_;
      const VectorType& lower_;

    };

  }

}

#endif
