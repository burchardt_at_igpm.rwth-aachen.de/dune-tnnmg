#ifndef POINTWISE_NONLINEARITY_HH
#define POINTWISE_NONLINEARITY_HH

/** \file
 * \brief Base class for nonlinearities that decouple in all scalar directions
 */

#include <dune/common/fmatrix.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/tnnmg/problem-classes/nonlinearity.hh>

/** \brief Base class for nonlinearities that decouple in all scalar directions
 *
 * The nonlinearities are assumed to be the sum of a smooth nonlinearity
 * and a nonsmooth nonlinearity. The latter must decouple in the onedimensional
 * Euclidean directions for TNNMG to work.  The PointwiseNonlinearity class
 * even assumes that *both* decouple in the Euclidean directions.
 *
 * To implement such a nonlinearity derive from this class and implement the
 * virtual methods. If a default implementation is present you do not need
 * to reimplement the method in general. However you can supply more
 * efficient implementations especially for non-decoupled nonlinearities.
 *
 * \tparam LocalVectorTypeTEMPLATE The local vector type
 * \tparam LocalMatrixTypeTEMPLATE The local matrix type of the Hessian matrix
 */
template < class LocalVectorTypeTEMPLATE=Dune::FieldVector<double,1>, class LocalMatrixTypeTEMPLATE=Dune::FieldMatrix<double,1,1> >
class PointwiseNonlinearity
: public Nonlinearity<LocalVectorTypeTEMPLATE,LocalMatrixTypeTEMPLATE>
{
public:
    typedef LocalVectorTypeTEMPLATE LocalVectorType;
    typedef LocalMatrixTypeTEMPLATE LocalMatrixType;

    //! The local block size
    static const int block_size = LocalVectorType::dimension;

    //! The global vector type
    typedef Dune::BlockVector< LocalVectorType > VectorType;

    //! The global matrix type
    typedef Dune::BCRSMatrix< LocalMatrixType > MatrixType;

    //! The index set for sparse matrix construction
    typedef Dune::MatrixIndexSet IndexSet;

    /** \brief Virtual destructor */
    virtual ~PointwiseNonlinearity() {};

    /** \brief Set the internal position vector u_pos to v.
     *
     * This method is empty, because it is only needed if the nonlinearity
     * does not decouple in the Euclidean directions.
     */
    virtual void setVector(const VectorType& v) {};

    /** \brief Update the (i,j)-th entry of the internal position vector u_pos to x.
     *
     * This method is empty, because it is only needed if the nonlinearity
     * does not decouple in the Euclidean directions.
     *
     * \param i global index
     * \param j local index
     * \param x new value of the entry (u_pos)_(i,j)
     */
    virtual void updateEntry(int i, double x, int j) {};

};

#endif
