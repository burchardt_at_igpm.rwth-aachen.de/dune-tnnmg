#ifndef SMALLFUNCTIONAL_HH
#define SMALLFUNCTIONAL_HH

/** \file
 * \brief Base class for a twice differentiable functional from \f$ \mathbb{R}^m\f$ to \f$ \mathbb{R} \f$
 */

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

/** \brief Base class for a twice differentiable functional from \f$ \mathbb{R}^m\f$ to \f$ \mathbb{R} \f$
 * \tparam dimension The domain space dimension
 */
template< int dimension >
class SmallFunctional
{
    public:
        typedef Dune::FieldVector<double, dimension> SmallVector;
        typedef Dune::FieldMatrix<double, dimension, dimension> SmallMatrix;

        virtual ~SmallFunctional() {};

        /** \brief Evaluate the functional at  \f$ v \in \mathbb{R}\f$ */
        virtual double operator()(const SmallVector v) const=0;
        
        /** \brief Evaluate the gradient vector at \f$ v \in \mathbb{R} \f$ */
        virtual SmallVector d(const SmallVector v) const=0;
        /** \brief Evaluate the Hesse matrix at \f$ v \in \mathbb{R} \f$ */
        virtual SmallMatrix d2(const SmallVector v) const=0;
};

#endif
