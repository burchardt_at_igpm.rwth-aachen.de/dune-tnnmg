set(TESTS
  smallfunctionaltest
  testminimalsurfacefunctional
)

foreach(_test ${TESTS})
  dune_add_test(SOURCES ${_test}.cc)
endforeach()
