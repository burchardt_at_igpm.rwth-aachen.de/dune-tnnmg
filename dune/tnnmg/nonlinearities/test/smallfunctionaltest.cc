#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <cassert>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/tnnmg/nonlinearities/inteuklid.hh>
#include <dune/tnnmg/nonlinearities/pnorm.hh>
#include <dune/tnnmg/nonlinearities/minimalsurfacefunctional.hh>

template <class T>
double
myDiff(T t1, const T& t2)
{
  t1 -= t2;
  return t1.infinity_norm();
}

template <int n>
void
myOuterProduct(const Dune::FieldVector<double, n> &v1,
               const Dune::FieldVector<double, n> &v2,
               Dune::FieldMatrix<double, n, n> &m)
{
  m = 0;
  for (size_t i=0; i<n; ++i) {
    m[i]  = v1;
    m[i] *= v2[i];
  }
}

int
main()
{
  // 1/2 * |x|^2
  typedef Euklid<2> MyEFunctional;
  // (|x|^2 + 1)^(1/2)
  typedef MinimalSurfaceFunctional<2> MyMFunctional;
  // 1/p (|x|^2 + c)^(p/2)
  typedef PNorm<2> MyPFunctional;
  typedef typename MyPFunctional::SmallVector MyVector;
  typedef typename MyPFunctional::SmallMatrix MyMatrix;

  // These two are identical
  MyPFunctional f_p1_1(1.0, 1.0);
  MyMFunctional f_m;

  // This is an antiderivative of the identity
  MyEFunctional f_e;

  std::cout << "Testing gradients" << std::endl;
  {
    MyVector v = { 1.0, 2.0 };

    // Since the two functions coincide, so should their derivatives.
    MyVector grad_p1_1 = f_p1_1.d(v);
    MyVector grad_m    = f_m.d(v);
    std::cout << grad_p1_1 << std::endl;
    std::cout << grad_m    << std::endl;
    assert(myDiff(grad_p1_1, grad_m) < 1e-10);

    // This should be exactly the original vector
    MyVector grad_e    = f_e.d(v);
    std::cout << grad_e    << std::endl;
    assert(myDiff(grad_e, v) < 1e-10);
  }
  std::cout << "Testing Hessians" << std::endl;
  {
    MyVector v = { 1.0, 2.0 };

    // Since the two functions coincide, so should their Hessians
    MyMatrix hess_p1_1 = f_p1_1.d2(v);
    MyMatrix hess_m    = f_m.d2(v);
    std::cout << hess_p1_1 << std::endl;
    std::cout << hess_m    << std::endl;
    assert(myDiff(hess_p1_1, hess_m) < 1e-10);

    // This should be the unit matrix
    MyMatrix hess_e    = f_e.d2(v);
    MyMatrix id = {
      { 1.0, 0.0 },
      { 0.0, 1.0 }
    };
    std::cout << hess_e    << std::endl;
    assert(myDiff(hess_e, id) < 1e-10);
  }
  std::cout << "Testing pnorm with p = 3, 4" << std::endl;
  {
    // We focus on the case c = 0 for simplicity.
    //
    // The map x |-> 1/p * |x|^p has the first derivative
    //
    // x |-> |x|^(p-2)*x
    //
    // and second derivative
    //
    // x |-> |x|^(p-2) id + (p-2)|x|^(p-4) x \otimes x.
    MyVector v = { 1.0, 2.0 };

    MyPFunctional f_p3_0(3.0, 0.0);
    double vnorm = v.two_norm();
    {
      MyVector expected = v;
      expected *= vnorm;
      std::cout << expected << std::endl;

      MyVector grad     = f_p3_0.d(v);
      std::cout << grad << std::endl;
      assert(myDiff(grad, expected) < 1e-10);
    }
    {
      MyMatrix expected;
      myOuterProduct(v, v, expected);
      expected /= vnorm;
      for (size_t i=0; i<MyVector::dimension; ++i)
        expected[i][i] += vnorm;
      std::cout << expected << std::endl;

      MyMatrix hess    = f_p3_0.d2(v);
      std::cout << hess << std::endl;
      assert(myDiff(hess, expected) < 1e-10);
    }
    MyPFunctional f_p4_0(4.0, 0.0);
    double vnorm2 = v.two_norm2();
    {
      MyVector expected = v;
      expected *= vnorm2;
      std::cout << expected << std::endl;

      MyVector grad     = f_p4_0.d(v);
      std::cout << grad << std::endl;
      assert(myDiff(grad, expected) < 1e-10);
    }
    {
      MyMatrix expected;
      myOuterProduct(v, v, expected);
      expected *= 2.0;
      for (size_t i=0; i<MyVector::dimension; ++i)
        expected[i][i] += vnorm2;
      std::cout << expected << std::endl;

      MyMatrix hess    = f_p4_0.d2(v);
      std::cout << hess << std::endl;
      assert(myDiff(hess, expected) < 1e-10);
    }
  }
}
