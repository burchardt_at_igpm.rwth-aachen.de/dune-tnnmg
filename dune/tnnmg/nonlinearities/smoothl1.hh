#ifndef SMOOTH_L1_NORM_HH
#define SMOOTH_L1_NORM_HH

#include <cmath>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include "smallfunctional.hh"

/**  \brief A SmallFunctional that implements 1/2 times the squared regularized \f$\ell^1\f$-norm.
  *
  *  \f$\Gamma:\mathbb{R}^d\rightarrow\mathbb{R};\quad\Gamma(x)=\frac{1}{2}\Vert x\Vert_{1,\varepsilon}^2=\frac{1}{2}\left(\sum_i\sqrt{x_i^2+\varepsilon \Vert x\Vert_2^2}\right)^2\f$
  *
  *  \tparam dimension the dimension of the domain
  */
template<int dimension>
class SmoothL1Square : public SmallFunctional< dimension >
{
    public:
        typedef typename SmallFunctional< dimension >::SmallVector SmallVector;
        typedef typename SmallFunctional< dimension >::SmallMatrix SmallMatrix;

        /**  \brief Constructor
          *
          *  \param epsilon the regularization parameter of the regularized \f$\ell^1\f$-norm
          */
        SmoothL1Square(const double epsilon) :
            epsilon_(epsilon)
        {}

        /**  \brief evaluation of \f$\Gamma\f$
          *
          *  \param v the point at which to evaluate \f$\Gamma\f$
          *  \returns \f$\Gamma(v)\f$
          */
        double operator()(const SmallVector v) const
        {
            double norm2_v = v.two_norm2();
            double gamma = 0.0;
            for (int i=0; i < SmallVector::dimension; ++i)
                gamma += std::sqrt(v[i]*v[i] + epsilon_*norm2_v);
            return 0.5*gamma*gamma;
        }

        /**   \brief returns the gradient of \f$\Gamma\f$ at v
          *
          *   Define \f$z_i(v):=\sqrt{v_i^2+\varepsilon \Vert v\Vert_2^2},\ \gamma(v):=\sum_i z_i(v)=\Vert v\Vert_{1,\varepsilon}\f$. Then \f$\Gamma(v)=\frac{1}{2}\gamma(v)^2\f$, 
          *   \f$\partial_m\Gamma(v)=\gamma(v)\cdot\partial_m\gamma(v)\f$ for \f$v\neq 0\f$, and \f$\partial_m\gamma(v)=\sum_i\partial_m z_i(v)\f$ as well as \f$\partial_m z_i(v)=z_i^{-1}v_m({\delta_i}_m+\varepsilon)\f$
          *
          *   \param v the point at which to evaluate the gradient
          *   \returns the gradient of \f$\Gamma\f$ at v
          */
        SmallVector d(const SmallVector v) const
        {
            double norm2_v = v.two_norm2();

            if (norm2_v==0.0)
                return v;

            SmallVector z;
            SmallVector gamma_di;

            double gamma = 0.0;
            for (int i=0; i < SmallVector::dimension; ++i)
            {
                z[i] = std::sqrt(v[i]*v[i] + epsilon_*norm2_v);
                gamma += z[i];
            }

            for (int i=0; i < SmallVector::dimension; ++i)
            {
                gamma_di[i] = 0.0;
                for (int k=0; k < SmallVector::dimension; ++k)
                {
                    if (i==k)
                        gamma_di[i] += (epsilon_+1.0)/z[k];
                    else
                        gamma_di[i] += epsilon_/z[k];
                }
                gamma_di[i] *= v[i];
            }

            gamma_di *= gamma;
            return gamma_di;
        }

        /**   \brief returns the Hessian of \f$\Gamma\f$ at v
          *
          *   With the definitions of the method docu for SmoothL1Squared::d we have
          *   \f$\partial^2_{m,n}\Gamma(v)=\partial_n\gamma(v)\cdot\partial_m\gamma(v) + \gamma(v)\cdot\partial^2_{m,n}\gamma(v)\f$ for \f$v\neq 0\f$, 
          *   and \f$\partial^2_{m,n}\gamma(v)={\delta_m}_n\sum_i z_i(v)^{-1}\left({\delta_m}_i+\varepsilon\right)-v_mv_n\sum_i\left(z(v)^{-3}\left({\delta_m}_i+\varepsilon\right)\left({\delta_i}_n+\varepsilon\right)\right)\f$
          *
          *   \param v the point at which to evaluate the Hessian
          *   \returns the Hessian of \f$\Gamma\f$ at v
          */
        SmallMatrix d2(const SmallVector v) const
        {
            double norm2_v = v.two_norm2();

            SmallMatrix H(0.0);
            if (norm2_v==0.0)
            {
                for (int i=0; i<dimension; ++i)
                    H[i][i] = 1.0;

                return H;
            }

            SmallVector z;
            SmallVector gamma_di;

            double gamma = 0.0;
            for (int i=0; i < SmallVector::dimension; ++i)
            {
                z[i] = std::sqrt(v[i]*v[i] + epsilon_*norm2_v);
                gamma += z[i];
            }

            for (int i=0; i < SmallVector::dimension; ++i)
            {
                gamma_di[i] = 0.0;
                for (int k=0; k < SmallVector::dimension; ++k)
                {
                    if (i==k)
                        gamma_di[i] += (epsilon_+1.0)/z[k];
                    else
                        gamma_di[i] += epsilon_/z[k];
                }
                gamma_di[i] *= v[i];
            }

            double gamma_dij;
            for (int i=0; i < SmallVector::dimension; ++i)
            {
                for (int j=0; j < SmallVector::dimension; ++j)
                {
                    gamma_dij = 0.0;

                    for (int k=0; k < SmallVector::dimension; ++k)
                    {
                        if ((i==k) and (j==k))
                            gamma_dij += (epsilon_+1.0)*(epsilon_+1.0)/(z[k]*z[k]*z[k]);
                        else if ((i==k) or (j==k))
                            gamma_dij += (epsilon_+1.0)*epsilon_/(z[k]*z[k]*z[k]);
                        else
                            gamma_dij += epsilon_*epsilon_/(z[k]*z[k]*z[k]);
                    }
                    gamma_dij *= -v[i]*v[j];

                    if (i==j)
                    {
                        for (int k=0; k < SmallVector::dimension; ++k)
                        {
                            if (i==k)
                                gamma_dij += (epsilon_+1.0)/z[k];
                            else
                                gamma_dij += epsilon_/z[k];
                        }
                    }

                    H[i][j] = gamma_di[i]*gamma_di[j] + gamma_dij*gamma;
                }
            }
            return H;
        }

    private:

        /** \brief the regularization parameter */
        double epsilon_;
};

#endif
