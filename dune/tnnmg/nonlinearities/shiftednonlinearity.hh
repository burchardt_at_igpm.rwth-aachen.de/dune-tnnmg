#ifndef SHIFTED_NONLINEARITY_HH
#define SHIFTED_NONLINEARITY_HH

#include <dune/common/fmatrix.hh>

#include "dune/tnnmg/problem-classes/nonlinearity.hh"



template < class LocalVectorType=Dune::FieldVector<double,1>, class LocalMatrixType=Dune::FieldMatrix<double,1,1> >
class ShiftedNonlinearity: public Nonlinearity<LocalVectorType, LocalMatrixType>
{
    public:
        typedef Nonlinearity<LocalVectorType, LocalMatrixType> NonlinearityType;

        using NonlinearityType::block_size;

        typedef typename NonlinearityType::VectorType VectorType;
        typedef typename NonlinearityType::MatrixType MatrixType;
        typedef typename NonlinearityType::IndexSet IndexSet;

        ShiftedNonlinearity(NonlinearityType& phi, const VectorType& shift) :
            phi_(phi),
            shift_(shift)
        {}

        double operator()(const VectorType& v) const
        {
            VectorType temp = shift_;
            temp += v;
            return phi_(temp);
        }

        void addGradient(const VectorType& v, VectorType& gradient) const
        {
            VectorType temp = shift_;
            temp += v;
            phi_.addGradient(temp, gradient);
        }

        void addHessian(const VectorType& v, MatrixType& hessian) const
        {
            VectorType temp = shift_;
            temp += v;
            phi_.addHessian(temp, hessian);
        }

        void addHessianIndices(IndexSet& indices) const
        {
            phi_.addHessianIndices(indices);
        }

        void directionalDomain(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& D)
        {
            VectorType temp = shift_;
            temp += u;
            phi_.directionalDomain(temp, v, D);
        }

        void directionalSubDiff(const VectorType& u, const VectorType& v, Dune::Solvers::Interval<double>& D)
        {
            VectorType temp = shift_;
            temp += u;
            phi_.directionalSubDiff(temp, v, D);
        }

        void setVector(const VectorType& v)
        {
            VectorType temp = shift_;
            temp += v;
            phi_.setVector(temp);
        }

        void updateEntry(int i, double x, int j)
        {
            phi_.updateEntry(i, shift_[i][j]+x, j);
        }

        void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
        {
            phi_.subDiff(i, shift_[i][j]+x, D, j);
        }

        double regularity(int i, double x, int j) const
        {
            return phi_.regularity(i, shift_[i][j]+x, j);
        }

        void domain(int i, Dune::Solvers::Interval<double>& D, int j) const
        {
            phi_.domain(i, D, j);
            D[0]-=shift_[i][j];
            D[1]-=shift_[i][j];
        }

    private:
        NonlinearityType& phi_;
        const VectorType& shift_;
};

#endif
