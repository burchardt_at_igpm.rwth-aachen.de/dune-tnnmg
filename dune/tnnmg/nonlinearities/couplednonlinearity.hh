// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_TNNMG_NONLINEARITIES_COUPLED_NONLINEARITY_HH
#define DUNE_TNNMG_NONLINEARITIES_COUPLED_NONLINEARITY_HH

#include <dune/common/fmatrix.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/tnnmg/problem-classes/nonlinearity.hh>
#include <dune/tnnmg/nonlinearities/smallfunctional.hh>



template <class GM, class GMT, int variants=1>
class CoupledNonlinearity:
    public Nonlinearity<
        Dune::FieldVector<double, GM::block_type::cols>,
        Dune::FieldMatrix<double, GM::block_type::cols, GM::block_type::cols> >
{
    static const int M = GM::block_type::rows;
    static const int N = GM::block_type::cols;

public:
    typedef Nonlinearity<Dune::FieldVector<double, N>, Dune::FieldMatrix<double, N, N> > Base;

    typedef typename Base::LocalVectorType LocalVectorType;
    typedef typename Base::IndexSet IndexSet;

    typedef typename Dune::BlockVector< Dune::FieldVector<double, N> > Vector;
    typedef typename Dune::BCRSMatrix< Dune::FieldMatrix<double, N, N> > Matrix;
    typedef GM GradientMatrix;
    typedef GMT TransposedGradientMatrix;
    typedef typename Dune::BlockVector< Dune::FieldVector< double, M > > GradientVector;
    typedef typename GradientVector::block_type LocalGradientVector;

    typedef typename Dune::FieldVector<double, M> FunctionalGradient;
    typedef typename Dune::FieldMatrix<double, M, M> FunctionalHessian;

    typedef Dune::BlockVector< Dune::FieldVector<double, 1> > WeightVector;

    typedef SmallFunctional<M> LocalAnisotropy;
    typedef typename std::shared_ptr<LocalAnisotropy> LocalAnisotropyPointer;

    CoupledNonlinearity(
            const GradientMatrix& B,
            const TransposedGradientMatrix& Bt,
            const WeightVector& weights,
            LocalAnisotropyPointer gamma,
            double TOL = 1e-15) :
        B_(B),
        Bt_(Bt),
        weights_(weights),
        TOL_(TOL)
    {
        for(int i=0; i<variants; ++i)
            gamma_[i] = gamma;
    }

    CoupledNonlinearity(
            const GradientMatrix& B,
            const TransposedGradientMatrix& Bt,
            const WeightVector& weights,
            std::array<LocalAnisotropyPointer, variants> gamma,
            double TOL = 1e-15) :
        B_(B),
        Bt_(Bt),
        weights_(weights),
        gamma_(gamma),
        TOL_(TOL)
    {}

    double operator()(const Vector& v) const
    {
        double r = 0.0;
        GradientVector Bv(B_.N());
        B_.mv(v,Bv);
        for (int i=0; i<Bv.size(); ++i)
        {
            r += (*gamma_[i % variants])(Bv[i]) * weights_[i];
        }
        return r;
    }

    void addGradient(const Vector& v, Vector& gradient) const
    {
        LocalGradientVector BiXv;
        FunctionalGradient Dgamma;

        for (int i=0; i<B_.N(); ++i)
        {
            BiXv  = 0.0;
            addRowXVector(B_[i], v, BiXv);

            Dgamma = gamma_[i % variants]->d(BiXv);
            Dgamma *= weights_[i];

            addRowTransposedXVector(B_[i], Dgamma, gradient);
        }
    }

    void addHessian(const Vector& v, Matrix& hessian) const
    {
        LocalGradientVector BiXv;
        FunctionalHessian DDgamma;

        for (int i=0; i<B_.N(); ++i)
        {
            BiXv = 0.0;
            addRowXVector(B_[i], v, BiXv);

            DDgamma = gamma_[i % variants]->d2(BiXv);
            DDgamma *= weights_[i];

            addRowTransposedXMatrixXRow(B_[i], DDgamma, B_[i], hessian);
        }
    }

    void addHessianIndices(IndexSet& indices) const
    {
        for(int i=0; i<weights_.size(); ++i)
            addIndicesRowTransposedXRow(B_[i], B_[i], indices);
    }

    void directionalSubDiff(const Vector& w, const Vector& v, Dune::Solvers::Interval<double>& D)
    {
        LocalGradientVector BiXw;
        FunctionalGradient Dgamma;

        typename Vector::block_type BijTXDgamma;

        double d = 0.0;
        for (int i=0; i<weights_.size(); ++i)
        {
            BiXw = 0.0;
            addRowXVector(B_[i], w, BiXw);

            Dgamma = gamma_[i % variants]->d(BiXw);
            Dgamma *= weights_[i];

            auto jIt  = B_[i].begin();
            auto jEnd = B_[i].end();
            for (; jIt!=jEnd; ++jIt)
            {
                (*jIt).mtv(Dgamma, BijTXDgamma);
                d += BijTXDgamma * v[jIt.index()];
            }
        }
        D[0] = d;
        D[1] = d;
    }

    void setVector(const Vector& v)
    {
        Bu_.resize(B_.N());
        u_ = v;
        B_.mv(u_, Bu_);
    }

    void updateEntry(int j, double uNew, int jj)
    {
        if (uNew==u_[j][jj])
            return;
        double uDiff = uNew - u_[j][jj];

        const auto& Bt_j = Bt_[j];
        auto iIt  = Bt_j.begin();
        auto iEnd = Bt_j.end();
        for(; iIt!=iEnd; ++iIt)
        {
            int i = iIt.index();

            const auto& Bt_ji = *iIt;
            const auto& Bt_ji_jj = Bt_ji[jj];

            auto iiIt = Bt_ji_jj.begin();
            auto iiEnd = Bt_ji_jj.end();
            for (; iiIt != iiEnd; ++iiIt)
                Bu_[i][iiIt.index()] += (*iiIt) * uDiff;
        }
        u_[j][jj] = uNew;
    }

    void subDiff(int j, double x, Dune::Solvers::Interval<double>& D, int jj) const
    {
        LocalGradientVector Bui;
        FunctionalGradient Dgamma;

        int i;
        double r = 0.0;
        double uDiff = x - u_[j][jj];

        const auto& Bt_j = Bt_[j];
        auto iIt  = Bt_j.begin();
        auto iEnd = Bt_j.end();
        for(; iIt!=iEnd; ++iIt)
        {
            i = iIt.index();

            const auto& Bt_ji = *iIt;
            const auto& Bt_ji_jj = Bt_ji[jj];

            auto iiIt = Bt_ji_jj.begin();
            auto iiEnd = Bt_ji_jj.end();
            if (iiIt!=iiEnd)
            {
                Bui = Bu_[i];
                for (; iiIt != iiEnd; ++iiIt)
                    Bui += (*iiIt) * uDiff;
                Dgamma = gamma_[i % variants]->d(Bui);
                Dgamma *= weights_[i];

                iiIt = Bt_ji_jj.begin();
                for (; iiIt != iiEnd; ++iiIt)
                    r += (*iiIt) * Dgamma[iiIt.index()];
            }
        }
        D[0] = r;
        D[1] = r;
    }

    double regularity(int j, double x, int jj) const
    {
        return 0.0;
    }

    void domain(int j, Dune::Solvers::Interval<double>& D, int jj) const
    {
        D[0] = -std::numeric_limits<double>::max();
        D[1] = std::numeric_limits<double>::max();
    }

protected:

    static void addRowXVector(const typename GradientMatrix::row_type& Brow, const Vector& x, typename GradientVector::block_type& y)
    {
        auto it  = Brow.begin();
        auto end = Brow.end();
        for (; it!=end; ++it)
            (*it).umv(x[it.index()], y);
    }


    static void addRowTransposedXVector(const typename GradientMatrix::row_type& Brow, const FunctionalGradient& x, Vector& y)
    {
        auto it  = Brow.begin();
        auto end = Brow.end();
        for (; it!=end; ++it)
            (*it).umtv(x, y[it.index()]);
    }

    static void addRowTransposedXMatrixXRow(const typename GradientMatrix::row_type& rowA, const FunctionalHessian& X, const typename GradientMatrix::row_type& rowB, Matrix& Y)
    {
        auto iIt = rowA.begin();
        auto iEnd = rowA.end();
        for(; iIt!=iEnd; ++iIt)
        {
            const auto& rowA_i = (*iIt);
            int i = iIt.index();

            auto jIt  = rowB.begin();
            auto jEnd = rowB.end();
            for(; jIt!=jEnd; ++jIt)
            {
                const auto& rowB_j = (*jIt);
                int j = jIt.index();

                // Y[i][j] += rowA_i' * X * rowB_j;
                typename Matrix::block_type& Y_ij = Y[i][j];
                for(int k=0; k<M; ++k)
                {
                    const auto& rowA_i_k = rowA_i[k];
                    for(int l=0; l<M; ++l)
                    {
                        const auto& rowB_j_l = rowB_j[l];

                        auto iiIt = rowA_i_k.begin();
                        auto iiEnd = rowA_i_k.end();
                        for (; iiIt != iiEnd; ++iiIt)
                        {
                            auto jjIt = rowB_j_l.begin();
                            auto jjEnd = rowB_j_l.end();
                            for (; jjIt != jjEnd; ++jjIt)
                                Y_ij[iiIt.index()][jjIt.index()] += (*iiIt) * X[k][l] * (*jjIt);
                        }
                    }
                }
            }
        }
    }

    static void addIndicesRowTransposedXRow(const typename GradientMatrix::row_type& rowA, const typename GradientMatrix::row_type& rowB, IndexSet& indices)
    {
        auto iIt = rowA.begin();
        auto iEnd = rowA.end();
        for(; iIt!=iEnd; ++iIt)
        {
            int i = iIt.index();

            auto jIt  = rowB.begin();
            auto jEnd = rowB.end();
            for(; jIt!=jEnd; ++jIt)
            {
                int j = jIt.index();

                // Y[i][i] += rowA_i' * X * rowB_j;
                indices.add(i, j);
            }
        }
    }

    GradientVector Bu_;
    const GradientMatrix& B_;
    const TransposedGradientMatrix& Bt_;
    const WeightVector& weights_;
    std::array<LocalAnisotropyPointer, variants> gamma_;
    const double TOL_;

    Vector u_;
};


#endif
