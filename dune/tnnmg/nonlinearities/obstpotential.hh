#ifndef OBSTPOTENTIAL_HH
#define OBSTPOTENTIAL_HH

#include "dune/tnnmg/problem-classes/pointwisenonlinearity.hh"

template < class LocalVectorType=Dune::FieldVector<double,1>, class LocalMatrixType=Dune::FieldMatrix<double,1,1> >
class ObstPotential: public PointwiseNonlinearity<LocalVectorType, LocalMatrixType>
{
    public:
        typedef Nonlinearity<LocalVectorType, LocalMatrixType> NonlinearityType;

        using NonlinearityType::block_size;

        typedef typename NonlinearityType::VectorType VectorType;
        typedef typename NonlinearityType::MatrixType MatrixType;
        typedef typename NonlinearityType::IndexSet IndexSet;


        ObstPotential(const VectorType& _lower, const VectorType& _upper, double _TOL = 1e-15) :
            TOL(_TOL),
            upper(_upper),
            lower(_lower)
        {}

        double operator()(const VectorType& v) const
        {
            for(size_t row=0; row<v.size(); ++row)
            {
                for(int j=0; j<block_size; ++j)
                {
                    if ((v[row][j] < lower[row][j]) or (v[row][j] > upper[row][j]))
                        return std::numeric_limits<double>::max();
                }
            }
            return 0.0;
        }

        void addGradient(const VectorType& v, VectorType& gradient) const {}
        void addHessian(const VectorType& v, MatrixType& hessian) const {}
        void addHessianIndices(IndexSet& indices) const {}

        void subDiff(int i, double x, Dune::Solvers::Interval<double>& D, int j) const
        {
            D[0] = 0.0;
            D[1] = 0.0;

            if (x < lower[i][j])
            {
                D[0] = -std::numeric_limits<double>::max();
                D[1] = -std::numeric_limits<double>::max();
            }
            if (x <= TOL + lower[i][j])
            {
                D[0] = -std::numeric_limits<double>::max();
            }
            if (x >= -TOL + upper[i][j])
            {
                D[1] = std::numeric_limits<double>::max();
            }
            if (x > upper[i][j])
            {
                D[0] = std::numeric_limits<double>::max();
                D[1] = std::numeric_limits<double>::max();
            }
            return;
        }

        double regularity(int i, double x, int j) const
        {
            if (x <= TOL + lower[i][j])
                return std::numeric_limits<double>::max();
            if (x >= -TOL + upper[i][j])
                return std::numeric_limits<double>::max();
            return 0.0;
        }

        void domain(int i, Dune::Solvers::Interval<double>& dom, int j) const
        {
            dom[0] = lower[i][j];
            dom[1] = upper[i][j];
            return;
        }

    private:
        const double TOL;

        const VectorType& upper;
        const VectorType& lower;

};

#endif

