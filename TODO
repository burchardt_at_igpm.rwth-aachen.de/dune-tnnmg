# Interface clean up

* OS proposes: rename method Nonlinearity::subDiff to Nonlinearity::subdifferential
* Make method Nonlinearity::directionalSubDiff const
  * The problem is, that the default implentation uses setVector()
* CG: Make Nonlinearity stateless
  * Currently one has to use setVector(u) and subDiff(i,x) in
    order to compute D_i f(u+e_i x) efficiently
  * Perhaps we could change subDiff(i,x) to compute D_i f(e_i x)
    and introduce a method returning a shifted version of f.
    This would make the interface much cleaner. However one
    would have to rewrite any Gauß-Seidel / Jacobi to
    act on corrections then (not a bad idea anyway).
* Implement support low rank matrix support for SimplexConstrained*
  * Not urgent, but would be nice
* Get rid of scalartnnmg
  * Since BlocknonlinearTNNMGProblem supports low-rank matrices
    we could now replace scalartnnmg.
  * It would be nice to have a simple drop-in replacement
* Remove the hyphen from the problem-classes directory name
* Rename method 'regularity' to 'irregularity'.
* Rename file inteuklid.hh and the class it contains:
  * The class name doesn't match the file name
  * It is not clear what the 'int' prefix is supposed to mean
  * In English, Herr Euklid is spelled Mister Euclid (with a 'c')
* Changes to SmallFunctional
  * Derive from VirtualFunction
  * Rename it to TwiceDifferentiableScalarFunction
  * Rename method d2 to evaluateHessian
* Make the field type a template parameter instead of hardwiring 'double'
* rename TruncatedNonsmoothNewtonMultigrid -> TNNMGStep
* Rename class PNorm. It actually implements |x|^p and NOT |x|_p.
* BlockNonlinearTNNMGStep should not inherit from GenericNonlinearGS
  * C: Why not?

# Modul layout
* Move nonlinearity.hh and pointwisenonlinearity.hh from common/ to nonlinearities/
